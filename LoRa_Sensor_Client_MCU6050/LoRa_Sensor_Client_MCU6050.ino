/***************LoRa_Sensor_Client_MCU6050****************
  Platform:Arduino Genuino Uno & Dragino LoRa sheild@433MHz
  Sensor:MCU6050 
   MCU6050---------Arduino Genuino Uno
    VCC                 ---VCC
    GND                 ---GND
    Tx                  ---Rx(pin 0)
    Rx                  ---Tx(pin 1)
  Modified 28 4 2019
  by Yinde Liu
****/
//room34563,sensorId=1013,sensorType=gyroscope fieldvalue0=120.4,fieldvalue1=48.9,fieldvalue2=3.0
#include <SPI.h>
#include <RH_RF95.h>
#define DATA_MAX_LEN  256   //每次向网关传输的最大数据量（单位Byte）
char DeviceID[]="1013";  //设备的ID
bool Reply_flag=false;
/***Begin----Customers Define Zone***/
#include <Wire.h>
#include <JY901.h>

/***End----Customers Define Zone***/

// Singleton instance of the radio driver
RH_RF95 rf95;

int led = A2;
float frequency = 433.0;

bool SensorInit()
{/***Begin----Customers Define Zone***/

 return 1;
 /***End----Customers Define Zone***/
}

uint8_t SensorGetData(uint8_t*Data)
{
 /***Begin----Customers Define Zone***/
 uint8_t Len=0;
 uint8_t head_char[]="room34563,sensorId=1013,sensorType=gyroscope fieldvalue0=";
 uint8_t temp_char[5];
 uint8_t temp[]=",fieldvalue1=";
 strcat(Data,head_char);
 float AngleX=(float)JY901.stcAngle.Angle[0]/32768*180;
 float AngleY=(float)JY901.stcAngle.Angle[1]/32768*180;
 float AngleZ=(float)JY901.stcAngle.Angle[2]/32768*180;
 //Serial.println(AngleX);
 dtostrf(AngleX,3,1,temp_char);
 strcat(Data,temp_char);
 
 strcat(Data,temp);
 dtostrf(AngleY,3,1,temp_char);
 strcat(Data,temp_char);
 
  uint8_t temp1[]=",fieldvalue2=";
  strcat(Data,temp1);
  dtostrf(AngleZ,3,1,temp_char);
  strcat(Data,temp_char);

  //Serial.println((char*)Data);
  return 1;
// DataAndLength[0]=Data;
 //DataAndLength[1]=sizeof(Data)+2;
 //Serial.print((char*)Data);
 //Serial.println((uint8_t)DataAndLength[1]);
 /***End----Customers Define Zone***/
 }
 
void setup() 
{
  pinMode(led, OUTPUT);
  //initial sensor
  SensorInit();
  Serial.begin(115200);
  while (!Serial) ; // Wait for serial port to be available
  Serial.println("Start Sketch");
  if (!rf95.init())
    Serial.println("init failed");
  // Setup ISM frequency
  rf95.setFrequency(frequency);
  // Setup Power,dBm
  rf95.setTxPower(13);
  // Defaults BW Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on
  Serial.print("Listening on frequency: ");
  Serial.println(frequency);
  rf95.setModeRx();
}

void loop()
{ uint8_t Data[DATA_MAX_LEN]={0};
  /*Serial.print("Angle:");
  Serial.print((float)JY901.stcAngle.Angle[0]/32768*180);
  Serial.print(" ");
  Serial.print((float)JY901.stcAngle.Angle[1]/32768*180);
  Serial.print(" ");
  Serial.println((float)JY901.stcAngle.Angle[2]/32768*180);*/
  SensorGetData(Data);
  //delay(500);
  uint8_t DataLen=strlen(Data)+1;
  
  //delay(500);
  if (rf95.available())
  {
    // Should be a message for us now   
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN]={0};
    uint8_t len = sizeof(buf);
    if (rf95.recv(buf, &len))
    {
      digitalWrite(led, HIGH);
      //RH_RF95::printBuffer("request:", buf, len);
      Serial.print("got request: ");
      Serial.println((char*)buf);
      Serial.print("RSSI: ");
      Serial.println(rf95.lastRssi(), DEC);
      // Send a reply
      Reply_flag=false;
      for(uint8_t i=0;i<len;i++)
        {if(buf[i]=='I')
          { Reply_flag=true;
              for(uint8_t j=1;j<=strlen(DeviceID);j++)
              {if(buf[i+j]!=DeviceID[j-1]) Reply_flag=false;
               }
           }
        }
        
      if(Reply_flag)
          {rf95.send((uint8_t*)Data,DataLen);
           rf95.waitPacketSent();
           Serial.println((char*)Data);
          }
          
      rf95.setModeRx();
      digitalWrite(led, LOW);
    }
    else
    {
      Serial.println("recv failed");
    }
  }
}

void serialEvent() 
{
  while (Serial.available()) 
  {
    JY901.CopeSerialData(Serial.read()); //Call JY901 data cope function
  }
}


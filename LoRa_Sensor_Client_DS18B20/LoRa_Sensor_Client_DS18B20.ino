/***************LoRa_Sensor_Client_DS18B20****************
  Platform:Arduino Genuino Uno & Dragino LoRa sheild@433MHz
  Sensor:DS18B20 Temperature sensor
   DS1820B20---------Arduino Genuino Uno
    VCC                 ---VCC
    GND                 ---GND
    DQ                  ---digital pin 3
  Modified 8 5 2019
  by Yinde Liu
****/

#include <SPI.h>
#include <RH_RF95.h>
#define DATA_MAX_LEN  256   //每次向网关传输的最大数据量（单位Byte）
char DeviceID[]="1012";  //设备的ID
bool Reply_flag=false;
/***Begin----Customers Define Zone***/
#include <OneWire.h>
#include <DallasTemperature.h>
#define ONE_WIRE_BUS 3  // 定义DS18B20数据口连接arduino的 3 脚
OneWire oneWire(ONE_WIRE_BUS);    // 初始连接在单总线上的单总线设备
DallasTemperature sensors(&oneWire);  //定义温度传感器实例

/***End----Customers Define Zone***/

// Singleton instance of the radio driver
RH_RF95 rf95;

int led = A2;
float frequency = 433.0;

bool SensorInit()
{/***Begin----Customers Define Zone***/
 sensors.begin();
 return 1;
 /***End----Customers Define Zone***/
}

uint8_t SensorGetData(uint8_t*Data)
{
 /***Begin----Customers Define Zone***/
 char head_char[]="room34563,sensorId=1012,sensorType=temperature fieldvalue0=";
 strcat(Data,head_char);
 char temp_char[5];
 //获取温度
 sensors.requestTemperatures(); 
 float Temp = sensors.getTempCByIndex(0);
 dtostrf(Temp,2,2,temp_char);
 strcat(Data,temp_char);
 return (strlen(Data)+1);
// DataAndLength[0]=Data;
 //DataAndLength[1]=sizeof(Data)+2;
 //Serial.print((char*)Data);
 //Serial.println((uint8_t)DataAndLength[1]);
 /***End----Customers Define Zone***/
 }
 
void setup() 
{
  pinMode(led, OUTPUT);
  //initial sensor
  SensorInit();
       
  Serial.begin(9600);
  while (!Serial) ; // Wait for serial port to be available
  Serial.println("Start Sketch");
  if (!rf95.init())
    Serial.println("init failed");
  // Setup ISM frequency
  rf95.setFrequency(frequency);
  // Setup Power,dBm
  rf95.setTxPower(13);
  // Defaults BW Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on
  Serial.print("Listening on frequency: ");
  Serial.println(frequency);
}

void loop()
{ uint8_t Data[DATA_MAX_LEN]={0};
  uint8_t DataLength=SensorGetData(Data);
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN]={0};
  uint8_t len = sizeof(buf);
  
  Serial.print("GET temperature!");
  //delay(500);
  if (rf95.available())
  {
    // Should be a message for us now   
    
    if (rf95.recv(buf, &len))
    { 
      digitalWrite(led, HIGH);
      //RH_RF95::printBuffer("request: ", buf, len);
      Serial.print("got request: ");
      Serial.println((char*)buf);
      Serial.print("RSSI: ");
      Serial.println(rf95.lastRssi(), DEC);
      //check received string with DeviceID ,if equal,Reply_flag will be set true
      Reply_flag=false;
      for(uint8_t i=0;i<len;i++)
        {if(buf[i]=='I')
          { Reply_flag=true;
              for(uint8_t j=1;j<=strlen(DeviceID);j++)
              {if(buf[i+j]!=DeviceID[j-1]) Reply_flag=false;
               }
           }
        }
       //if Reply_flag is true,it will send sensor data to gateway
      if(Reply_flag)
          {rf95.send((uint8_t*)Data,DataLength);
          Serial.println((char*)Data);
          Serial.println(DataLength);
          rf95.waitPacketSent();
          }
      rf95.setModeRx();
      digitalWrite(led, LOW); 
    }
    else
    {
      Serial.println("recv failed");
    }
  }
}



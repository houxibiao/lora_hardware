/***************LoRa_Sensor_GateWay_LG01****************
  Platform:LG01
  Program to get sensor data from LoRa_Sensor_Client_xxx and transfer it to Server...
  ...through Internet.
  It was designed to work with LoRa_Sensor_Clint_xxx.
  Modified 11 5 2019
  by Yinde Liu
****/

//If you use Dragino IoT Mesh Firmware, uncomment below lines.
//For product: LG01. 
#define BAUDRATE 115200

//If you use Dragino Yun Mesh Firmware , uncomment below lines. 
//#define BAUDRATE 250000

#include <Console.h>
#include <SPI.h>
#include <Process.h>
#include <RH_RF95.h>

// Singleton instance of the radio driver
RH_RF95 rf95;

float frequency = 433.0;

#define ID_LEN 4          //节点ID长度（单位Byte）
#define MAX_DEVICE_NUM 9 //最大支持的节点数
#define led A2
int Client_num=0; //被轮询设备数量
char ID[MAX_DEVICE_NUM*(ID_LEN+1)+1]={0};//被轮询设备ID列表  //MAX_DEVICE_NUM*(ID_LEN+1)+1
char ID_buffer[ID_LEN+2]={0};
uint8_t buf[RH_RF95_MAX_MESSAGE_LEN]={1};
//void GetIDlist(void);
uint8_t RegetIDlist_counter=0;
#define RegetIDlist_interval 10   //每轮询RegetIDlist_interval次，重新从服务器下载节点ID列表
uint8_t i=0;
bool ledmod=0;

void setup() 
{
  Bridge.begin(BAUDRATE);
  Console.begin();
  //while (!Console) ; // Wait for console port to be available
  //GetIDlist();
  Console.println("Start Sketch");
  if (!rf95.init())
    Console.println("init failed");
  // Setup ISM frequency
  rf95.setFrequency(frequency);
  // Setup Power,dBm
  rf95.setTxPower(13);
  // Defaults BW Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on
   pinMode(led,OUTPUT);
}

void loop()
{  
  // Send a message to LoRa Server
  Process p;
  uint8_t j=0;
  RegetIDlist_counter++;
  
  /******更行节点列表*********/
  if(RegetIDlist_counter>RegetIDlist_interval)
     {digitalWrite(led, ledmod); 
      ledmod=!ledmod;  
      RegetIDlist_counter=0;
      //request sensors information
      p.begin("curl");
      p.addParameter("-G");
      p.addParameter("http://123.56.20.55:8082/sensors/getSensors");
      p.run();
      bool sb_valid=false;
      bool b_valid=false;  
      Client_num=0;
      i=0;
      //upack received data,retrieve sensor id list
      while (p.available() > 0) 
          {
            if(Client_num>=MAX_DEVICE_NUM)  
              {Console.print("Warning:Devices'number exceeds the limit!");
               break;
              }
            char c = p.read();
            if(c=='[') sb_valid=true;
            if(sb_valid&&(c=='{')) b_valid=true;
            if(b_valid&&(c==':'))
                {do{c = p.read();
                    ID[i]=c;
                    i++;
                    }while(c!=',');
                 Client_num++;
                 b_valid=false;
                }
        //Console.print(c);
          }
      ID[i]=0;
      Console.println((char*)ID);
     }

/************************轮询************************/
  Console.println((char*)ID);
  for(i=0;i<Client_num;i++)
  {
   ID_buffer[0]='I';
   uint8_t k=1;
    for(;ID[j]!=',';j++)
      {ID_buffer[k]=ID[j];
       k++;
      }
   ID_buffer[k]=0; 
   j++;
   //Console.println((int)Client_num);
   //Console.println((int)i);
   Console.println((char*)ID_buffer);
   rf95.send((char*)ID_buffer, strlen(ID_buffer)); 
   rf95.waitPacketSent();
  // Now wait for a reply
  rf95.setModeRx();
  Console.println("Request to Sensor Sent!");
 
  uint8_t len = sizeof(buf);
  delay(800);
  if (rf95.available())
  { 
    // Should be a reply message for us now   
    if (rf95.recv(buf, &len))
   {
      Console.print("Got reply from Sensor:");
      Console.println((char*)buf);
      //Console.print("RSSI: ");
      //Console.println(rf95.lastRssi(), DEC);
        
      //transfer data to Server by HTTP POST  
         //Process p;
         p.begin("curl");
         p.addParameter("-i");
         p.addParameter("-XPOST");
         p.addParameter("123.56.20.55:8086/write?db=yuntest&u=hou&p=Hou13734");
         p.addParameter("--data-binary");
         p.addParameter(buf);
         p.run();
    
         while (p.available() > 0) {
            char c = p.read();
            Console.print(c);
         }
         for(int m=0;m<RH_RF95_MAX_MESSAGE_LEN;m++) {buf[m]=1;}
    }
    else
    {
      Console.println("recv failed");
    }
  }
  else
  {
    Console.println("No reply, is Sensor running?");
  }
  //delay(500);
  }
  Console.flush();
}



